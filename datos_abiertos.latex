\documentclass{article}

\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage[colorlinks=true,linkcolor=blue,urlcolor=blue]{hyperref}
\usepackage{framed}

\title{Datos abiertos}
\author{Jaime López}
\date{Abril de 2014}

\begin{document}

\maketitle

\section{Definiciones}

Datos abiertos: Una pieza de datos o contenidos que cualquiera -- persona o autómata -- es libre de usar, reusar y distribuir sin restricción, salvo, a lo sumo, la atribución de la fuente.\footnote{Adaptado de Open Definition. Enlace: \url{http://opendefinition.org/} (consultado el 21 de abril de 2014).}

Datos abiertos gubernamentales: Son datos abiertos producidos por el gobierno. Generalmente se acepta que se trata de datos producidos o recolectados en el quehacer del gobierno. Es un subconjunto de la información del sector público.\footnote{Adaptado del glosario del Open Data Handbook. Enlace: \url{http://opendatahandbook.org/en/glossary.html} (consultado el 21 de abril de 2014).}

\section{Principios}

Los datos abiertos son\footnote{Adaptado de la propuesta \textit{Open Data Government Principles}. Enlace: \url{https://public.resource.org/8_principles.html} (consultado el 21 de abril de 2014).}:

\begin{itemize}

\item Completos. Todos los datos públicos están disponibles.

\item Primarios. Son recolectados de la fuente, en su forma original y con el mayor nivel posible de desagregación.

\item Actualizados. Los datos públicos están disponibles tan pronto como es posible para preservar el valor de los datos.

\item Accesibles. Los datos están disponibles para el rango más amplio de usuarios y el rango más amplio de propósitos posible.

\item Procesables. Los datos están razonablemente estructurados para permitir su procesamiento automatizado.

\item No discriminatorios. Los datos están disponibles para cualquiera, sin que se requiera registro previo o acreditación de motivos.

\item No propietarios. Los datos están disponibles en formatos sobre los cuales ninguna entidad tiene control exclusivo.

\item De licencia libre: Los datos no están sujetos a ninguna regulación de copyright, patentes o marcas registradas.

\end{itemize}

\section{Inicaitivas}

\begin{itemize}

\item Open Government Partnership: Cuenta con un grupo de trabajo sobre datos abiertos. La iniciativa funciona con planes formulados en forma participativa por los gobiernos y un mecanismo independiente de revisión.\footnote{Enlace: \url{http://www.opengovpartnership.org/}}

\item Infoútil: Es una iniciativa del gobierno salvadoreño para compartir bases de datos.\footnote{Enlace: \url{http://infoutil.gobiernoabierto.gob.sv/}}

\item datos.egob.sv: Es otra iniciativa gubernamental para compartir bases de datos.\footnote{Enlace: \url{http://datos.egob.sv/}}

\end{itemize}

\section{Base normativa}

Ley de Acceso a la Información Pública incluye el acceso a bases de datos:

\begin{quote}
Información pública: es aquella en poder de los entes obligados contenida en
documentos, datos, bases de datos, comunicaciones y todo tipo de registros que
documenten el ejercicio de sus facultades o actividades. (artículo 6, literal
c) \end{quote}

\end{document}
