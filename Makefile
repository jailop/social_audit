SOURCES = \
	contraloria_antecedentes.latex \
	contraloria_ejercicio.latex \
	datos_abiertos.latex \
	graficas_calculos.latex \

TARGETS = $(SOURCES:.latex=.pdf)

DIRS = graphs plots

.PHONY: $(DIRS)

all: $(DIRS) $(TARGETS)

%.pdf: %.latex
	pdflatex $<	
	pdflatex $<	

$(DIRS): 
	cd $@; $(MAKE)

clean: clean_dirs 
	rm -f *.pdf
	rm -f *.aux *.log *.out  

clean_dirs: $(DIRS)
	cd $<; $(MAKE) clean
